import vueRouter from 'vue-router';
import Vue from 'vue';

Vue.use(vueRouter);

import Index from "./views/index";
import Blog from "./views/blog";
import Post from "./views/Post";
import CreatePost from "./views/CreatePost";
//import Post from "./components/Blog/Post";

const routes = [
    {
        path: "/",
        component: Index
    },
    {
        path: "/blog",
        component: Blog
    },
    {
        path: "/post/:id",
        component: Post
    },
    /*{
        path: "/read-post/:id",
        component: PostLink
    },*/
    {
        path: "/create",
        component: CreatePost
    }
];

export default new vueRouter(
    {
        mode: "history",
        routes //routes = routes

});
